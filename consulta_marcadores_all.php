<?php
$sql = "SELECT b.id, b.nombre, CONCAT(a.Lat,',',a.Lng) as LatLng, b.especialidad, b.telefono, b.sexo as sexo, b.level as level, b.edad as edad, a.enable, a.tespera, b.imagen as imagen,b.imagen_cover as imagen_cover, b.imagen_cover as imagen_cover, b.direccion as direccion, (SELECT AVG(rating) FROM ma_review WHERE id = a.id) as rate, b.enable as activo FROM ma_trader_location as a, ma_users as b WHERE a.id = b.id"; 


require_once('conn_db.php'); 


function getArraySQL($sql){
    //Creamos la conexión con la función anterior
    $conexion = connectDB();

    //generamos la consulta

        mysqli_set_charset($conexion, "utf8"); //formato de datos utf8

    if(!$result = mysqli_query($conexion, $sql)) die(); //si la conexión cancelar programa

    $rawdata = array(); //creamos un array

    //guardamos en un array multidimensional todos los datos de la consulta
    $i=0;

    while($row = mysqli_fetch_array($result))
    {
        $rawdata[$i] = $row;
        $i++;
    }

    disconnectDB($conexion); //desconectamos la base de datos

    return $rawdata; //devolvemos el array
}

        $myArray = getArraySQL($sql);
        echo json_encode($myArray);
?>